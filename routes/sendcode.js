var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./routes/codes.db');
var request = require('request');

var timestamp = function () {
  return Math.floor(Date.now() / 1000);
};


// tables: codes10, codes15, codes20
// (code, used, email, sent, subscribe, time)
// CREATE TABLE codes10 (code TEXT, used INTEGER, email TEXT, sent INTEGER, subscribe INTEGER, time TEXT);

// sqlite> .mode csv
// sqlite> .import test.csv foo

// INCOMING: http://magic.conceptclub.ru/sendcode?email=arseny@deasign.ru&subscribe=true&matches=4
// OUTCOMING: localhost:8888?needsub=0&email=arseny@deasign.ru&promo=AAA-BBB-CCC

router.get('/', function(req, res, next) {

  var isAjax = req.xhr;

  // Check for AJAX and params
  if (!isAjax || !req.query.email || !req.query.subscribe || !req.query.type) {
    console.log('ERROR: some parameters are missing / not an AJAX call');
    res.send({ sent: false });
    return;
  }


  console.log('STATUS: staring to query database');
  db.serialize(function() {
    db.get('select * from codes where used=0 limit 1', function (err, row) {

      if (!err) {
        // Found code
        console.log('STATUS: found code: ' + row.code);

        var types = {
          'A': 1,
          'B': 2,
          'C': 3,
          'D': 4,
        };

        var options = {
          url: 'http://projects.mars.7host.ru/conceptclub/konkurs/index.php',
          qs: {
            emails: req.query.email,
            promo: row.code,
            letter: types[req.query.type]
          }
        };

        // Handler for mailsender
        var handler = function (error, response, body) {
          if (!error && response.statusCode == 200) {

            db.run('UPDATE codes SET used = 1, email = $email, sent = $sent, subscribe = $subscribe, time = $time WHERE code = $code', {
              $email: req.query.email,
              $sent: true, //TODO
              $subscribe: JSON.parse(req.query.subscribe),
              $time: timestamp(),
              $code: row.code
            });

            res.send({ sent: true, code: row.code });

          } else {
            console.log('ERROR: bad response from localhost' + error);
            res.send({ sent: false });
          }
        };

        // Sending request to mailsender
        request(options, handler);

        /**
         *
         * [15.08.17, 11:03:48] Elian Golikova: Пример переменных и значений:
         • emails="kopostra@mail.ru" - емейл подписчика
         • promo ="903-615-640" - промо-код на скидку
         • letter=2; номер письма с рекомендациями 1,2,3,4
         [15.08.17, 11:04:11] Алексей: ага
         [15.08.17, 11:04:12] Elian Golikova: Ссылка на скрипт с примером вызова
         http://projects.mars.7host.ru/conceptclub/konkurs/index.php?emails=test@mail.ru&promo=903-615-640&letter=1
         *
         */


        // Preparing request for mailsender
        /*var options = {
          url: 'http://localhost:8888',
          qs: {
            needsub: JSON.parse(req.query.subscribe) ? 1 : 0,
            email: req.query.email,
            promo: row.code,
            discount: discountValue
          }
        };

        // Handler for mailsender
        var handler = function (error, response, body) {
          if (!error && response.statusCode == 200) {
            var sent = JSON.parse(body).sent;

            res.send({ sent: sent });

            db.run(sqlQuery.update, {
              $email: req.query.email,
              $sent: (sent),
              $subscribe: JSON.parse(req.query.subscribe),
              $time: timestamp(),
              $code: row.code
            });

          } else {
            console.log('ERROR: bad response from localhost' + error);
            res.send({ sent: false });
          }
        };

        // Sending request to mailsender
        request(options, handler);*/

      } else {
        // Can't found code
        console.log('ERROR: can\'t found code: ' + error);
        res.send({ sent: false });
      }
    });
  });
});



module.exports = router;
