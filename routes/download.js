var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./routes/codes.db');
var request = require('request');
var fs = require('fs');

var timestamp = function () {
  return new Date().getTime();
};


// tables: codes10, codes15, codes20
// (code, used, email, sent, subscribe, time)
// CREATE TABLE codes10 (code TEXT, used INTEGER, email TEXT, sent INTEGER, subscribe INTEGER, time TEXT);

// sqlite> .mode csv
// sqlite> .import test.csv foo

// INCOMING: http://magic.conceptclub.ru/sendcode?email=arseny@deasign.ru&subscribe=true&matches=4
// OUTCOMING: localhost:8888?needsub=0&email=arseny@deasign.ru&promo=AAA-BBB-CCC


// http://127.0.0.1:1337/download?from=2017-08-10&to=2017-08-12
router.get('/', function (req, res, next) {

  //req.query.from

  var select = '',
      from, to;

  if (req.query.from) {
    //var queryFrom = req.query.from.split('-');
    from = new Date(req.query.from + ' 00:00:00').getTime();

    //from = req.query.from;
  }

  if (req.query.to) {
    //var queryTo = req.query.to.split('-');
    to = new Date(req.query.to + ' 23:59:00').getTime();

    //to = req.query.to;
  }

  if (from && to) {
    select = 'select * from codes where time != "" and time >=' + from + ' and time <=' + to;
  }
  else if (from && !to) {
    select = 'select * from codes where time != "" and time >=' + from;
  }
  else if (!from && to) {
    select = 'select * from codes where time != "" and time <=' + to;
  }
  else {
    select = 'select * from codes where time != ""';
  }


  db.serialize(function () {

    db.all(select, function (err, rows) {
      if (!err) {
        // Found code
        var fileName = timestamp();
        var file = fs.createWriteStream('public/storage/csv/' + fileName + '.csv');
        file.on('error', function(err) { /* error handling */ });

        rows.forEach(function (row) {
          console.log(row.code);
          file.write(row.code + '\n');
        });

        file.end();
      }
    });

    /*db.all('select * from codes10 limit 1', function (err, row) {
      if (!err) {
        // Found code
        console.log('STATUS: found code: ' + row);


      } else {
        // Can't found code
        console.log('ERROR: can\'t found code: ' + error);
        res.send({ sent: false });
      }
    });*/
  });



  //arr.forEach(function(v) { file.write(v.join(', ') + '\n'); });
  //file.write('123' + '\n');
  //file.end();

  res.send(select);
  //res.redirect('storage/csv/array.csv');

});


module.exports = router;