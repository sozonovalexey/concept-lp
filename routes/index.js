var express = require('express');
var router = express.Router();
// var _ = require('underscore');

/* GET home page. */
router.get('/:id?', function(req, res, next) {



  // Filling with defaults values
  var params = {
    'og-title': 'Новый сезон – новая ты! Выиграй 10 000 рублей на шопинг в Concept Club!',
    'og-name': '',
    'og-desc': 'Переворачивай волшебные карты, находи пары одинаковых и получи скидку на новогодний шоппинг в интернет-магазине, а также предсказание на Новый год!',
    'og-url': 'https://new.conceptclub.ru/',
    //'og-image': 'http://magic.conceptclub.ru/images/vk/' + req.params.id + '.jpg'
    'og-image': 'https://new.conceptclub.ru/images/share/share_A.jpg'
  };
  // Render view
  res.render('index', params);
});

module.exports = router;
