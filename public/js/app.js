/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 *
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);


'use strict';

var Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		var url  = 'https://vk.com/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		//url += '&description=' + encodeURIComponent(text); // больше не поддерживается
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		console.log(url);
		Share.popup(url);
	},

	popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};

var checkEmail = function(input) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return input.match(re);
};

// Required by YouTube API
var onYouTubeIframeAPIReady;

var initGame = function () {

  var ContentProvider = function (numberOfPairs) {

    var images = [
      '/images/cards/1-A.jpg',
      '/images/cards/1-B.jpg',
      '/images/cards/1-C.jpg',
      '/images/cards/1-D.jpg',
      '/images/cards/2-A.jpg',
      '/images/cards/2-B.jpg',
      '/images/cards/2-C.jpg',
      '/images/cards/2-D.jpg',
      '/images/cards/3-A.jpg',
      '/images/cards/3-B.jpg',
      '/images/cards/3-C.jpg',
      '/images/cards/3-D.jpg',
    ];

    images = _.shuffle(images);

    // Random seq of paired ids
    //var ids = _.chain(_.range(numberOfPairs)).zip(_.range(numberOfPairs)).flatten().shuffle().value();

    var ids = _.shuffle([0,1,2,3,4,5,6,7,8,9,10,11]);

    this.getContentId = function () {
      var first = _.first(ids);
      ids = _.rest(ids);
      return first;
    };

    this.getImage = function (id) {
      return images[id];
    };

    this.reset = function () {
      images = _.shuffle(images);
      //ids = _.chain(_.range(numberOfPairs)).zip(_.range(numberOfPairs)).flatten().shuffle().value();
      ids = _.shuffle([0,1,2,3,4,5,6,7,8,9,10,11]);
    }
  };


  var Card = function (gameController, contentProvider) {

    var card = this;

    this.id = _.uniqueId();
    this.opened = false;
    this.matched = false;
    this.contentId = contentProvider.getContentId();
    this.imageUrl = contentProvider.getImage(this.contentId);

    /**
     * Логика: если пользователь нажал на карту, то записываем номер этого клика. Максимум может быть 3 клика.
     */
    this.clicked = false;

    var $element = $(_.template('<div class="small-3 medium-2 large-2 columns"> <div class="container"> <div class="card"> <div class="front"></div> <div class="back" style="background-image: url(\'<%= imageUrl %>\');"><div class="content"></div></div> </div> </div> </div>')(this));
    var $card = $element.find('.card');

    this.create = function ($parent) {

      // Adding card to the DOM
      $parent.append($element);
      // Capturing id

      // Adding callback to click event
      $card.on('click', function() {
        // Just notify gameController about this click
        gameController.cardClicked(card.id);
      });
      // Getting controller to know about this card
      gameController.addCard(card);
    };

    this.open = function () {
      this.opened = true;
      $card.addClass('opened');
    };

    this.close = function () {
      this.opened = false;
      $card.removeClass('opened');
    };

    this.makeMatched = function () {
      this.matched = true;
      $card.addClass('matched');
    };

    this.reset = function() {
      this.opened = false;
      $card.removeClass('opened');
      this.matched = false;
      this.clicked = false;
      $card.removeClass('matched');
      this.contentId = contentProvider.getContentId();
      this.imageUrl = contentProvider.getImage(this.contentId);
      var $oldElement = $element;
      $element = $(_.template('<div class="small-3 medium-2 large-2 columns"> <div class="container"> <div class="card"> <div class="front"></div> <div class="back" style="background-image: url(\'<%= imageUrl %>\');"><div class="content"></div></div> </div> </div> </div>')(this));
      $oldElement.replaceWith($element);
      $card = $element.find('.card');
      // Adding callback to click event
      $card.on('click', function() {
        // Just notify gameController about this click
        gameController.cardClicked(card.id);
      });
    };

    this.click = function (clickNumber) {
      this.clicked = clickNumber;
      $card.addClass('fade out');
    };

    this.getLetter = function () {
      var image = this.imageUrl;
      return image.charAt(image.length - 5);
    };

  };




  var GameController = function (cp) {

    var self = this;

    var cards = {};

    var windowController = {
      element: $('.game'),
      setHeight: function (height) {
        //this.element.css('height', height);
      },
      resetHeight: function () {
        //this.element.css('height', 'auto');
      }
    };

    var timerController = {
      startTime: 30,
      currentTime: 30,
      // startTime: 3,
      // currentTime: 3,
      timerId: -1,
      element: $('#game-time'),
      init: function () {
        this.currentTime = this.startTime;
        this.element.html(':' + this.currentTime);
      },
      startTimer: function () {
        var timer = this;
        this.timerId = setInterval(function () {
          timer.update();
        }, 1000);
      },
      stopTimer: function () {
        clearInterval(this.timerId);
      },
      update: function () {
        if (this.currentTime > 0) {
          this.currentTime -= 1;
          this.element.html(':' + this.currentTime);
        } else {
          this.stopTimer();
          self.endGame();
        }
      }
    };

    var matchesController = {
      matches: 0,
      element: $('#game-matches'),
      // discount: [0, 0, 10, 10, 15, 15, 20],
      discount: [0, 10, 15, 20, 20, 20, 20],
      discountElement: $('#game-discount'),
      init: function () {
        this.matches = 0;
        this.updateView();
      },
      addMatch: function () {
        this.matches += 1;
        if (this.matches == 3) {
					setTimeout(function () {
						self.endGame();
					}, 300);
        }
        this.updateView();
      },
      getDiscount: function () {
        return this.discount[this.matches];
      },
      updateView: function () {
        this.element.html(this.matches);
        this.discountElement.html(this.getDiscount() + '%');
      }
    };

    var codeScreenController = {
      element: $('#game-code-screen'),
			enabled: true,
      show: function () {
				this.element.removeClass('hide-form-active');
        //$('#game-code-discount').html(matchesController.getDiscount() + '%');
        $('#game-code-discount').html('10%');
        //windowController.setHeight(this.element.height());

        _.delay(function () {
          $('.code input').focus();
        }, 700);

        var $element = this.element;
        $('.js-screen').hide(1, function () {
          $('.js-screen').removeClass('active');
          $element.show();
          $element.addClass('active');
        });
      },
      hide: function () {
        this.element.hide();
        this.element.removeClass('active');
      },
      init: function () {
        var self = this;

        $('#game-back-to-prediction').on('click', function (e) {
          e.preventDefault();
          //self.hide();
          predictionScreenController.show();
        });

        this.element.keyup(function () {
					if (checkEmail($('#game-data-email').val())) {
          	$(this).removeClass('error');
					}
        });

        this.element.find('form').submit(function () {

          if(!$('#game-data-agreement').is(':checked')) {
           alert('Вам нужно принять пользовательское соглашение о конфиденциальности!');
           return false;
          }

          var params = {
            email: $('#game-data-email').val(),
            subscribe: $('#game-data-subscribe').is(':checked'),
            type: $('.js-type-text.active').data('type')
            //matches: matchesController.matches,
          };

          if (!checkEmail(params.email)) {
            $('#game-data-email').focus();
            return self.element.addClass('error');
          } else {
            self.element.removeClass('error');
          }

          var successHandler = function(response) {
						self.enabled = true;
            if (response['sent']) {
              finalScreenController.show();
              $('.js-promo').html(response['code']);
            } else {
              finalScreenController.showError();
              $('.js-promo').html('0000-0000-0000');
            }
          };

					if (self.enabled) {
						self.enabled = false;
						console.log('sending');
						$.ajax({
	            url: '/sendcode',
	            data: params,
	            success: successHandler
	          });

            // var response = [];
            // response['sent'] = true;
            // successHandler(response)
					}

          // GA goal
          ga('send', {
            'hitType': 'event',
            'eventCategory': 'game',
            'eventAction': 'getcode',
            'eventValue': matchesController.matches
          });
        });

        return this;
      }
    }.init();

    var predictionScreenController = {
      element: $('#game-prediction-screen'),
			predId: -1,
			reset: function () {
				this.predId = -1;
			},
      prepare: function (type) { // Prepare results
        $('.js-title').hide().removeClass('active');
        $('.js-title[data-type="' + type + '"]').show().addClass('active');

        $('.js-type-text').hide().removeClass('active');
        $('.js-type-text[data-type="' + type + '"]').show().addClass('active');

        $('.js-result').hide().removeClass('active');
        $('.js-result[data-type="' + type + '"]').show().addClass('active');
      },
      show: function () {

        // $('#game-share-prediction').attr('href', shareUrl);
        $('#game-back-to-prediction').show();

        //windowController.setHeight(this.element.height());

        var $element = this.element;
        $('.js-screen').hide(1, function () {
          //$element.addClass('active');
          $('.js-screen').removeClass('active');
          $element.show();
          $element.addClass('active');
        });

        return;

        $('#game-result-time').html(':' + timerController.currentTime);
        $('#game-result-matches').html(':' + matchesController.matches);
        $('#game-result-discount').html(matchesController.getDiscount() + '%');
				if (this.predId < 0) {
        	this.predId = _.random(0, predictions.length - 1);
				}
				var predId = this.predId;
        //$('#game-prediction-text').html(predictions[predId]);
        /*var shareUrlTemplate = 'https://vk.com/share.php?url=<%=url%>&title=<%=title%>&description=<%=description%>&image=<%=image%>';
        var shareUrl = (_.template(shareUrlTemplate))({
          url: 'http://magic.conceptclub.ru/',
          description: '%23magic_conceptclub ' + predictionsClean[predId],
          title: 'Играй и получай подарки от Concept Сlub!',
          image: 'http://magic.conceptclub.ru/images/vk/' + predId + '.jpg'
        });*/

      },
      hide: function () {
        this.element.hide();
        this.element.removeClass('active');
      },
      activateNextScreen: function () {
        var thisScreen = this;
        codeScreenController.show();
        /*_.delay(function () {
          thisScreen.hide();
        }, 1000);*/
      },
      init: function () {

        // Расшарим результат
        $('#game-share-prediction').on('click', function () {

            $('#game-back-to-prediction').hide();

            // GA goal
            ga('send', {
                'hitType': 'event',
                'eventCategory': 'game',
                'eventAction': 'share'
            });

            var shareTexts = {
              'A': {
                'text': 'Твой #новыйобраз_conceptclub – СПОРТИВНАЯ ШТУЧКА! Выиграй 10 000 рублей на новый образ от Concept Club!',
              },
              'B': {
                'text': 'Твой #новыйобраз_conceptclub – НАСТОЯЩАЯ ЛЕДИ! Выиграй 10 000 рублей на новый образ от Concept Club!',
              },
              'C': {
                'text': 'Твой #новыйобраз_conceptclub – ХРУПКАЯ МЕЧТАТЕЛЬНИЦА! Выиграй 10 000 рублей на новый образ от Concept Club!',
              },
              'D': {
                'text': 'Твой #новыйобраз_conceptclub – РОКОВАЯ КРАСОТКА! Выиграй 10 000 рублей на новый образ от Concept Club!',
              },
            },
            type = $('.js-type-text.active').data('type');

            var url = location.protocol + '//' + window.location.host + window.location.pathname,
                title = 'Выиграй 10 000 рублей на новый образ от Concept Club!',
                text = "Смотри работы фото-блогеров и участвуй в конкурсе.",
                img = url + 'images/share/share_' + type + '.jpg';

            console.log(shareTexts[type]['text']);
            console.log(img);

            //Share.vkontakte(url, shareTexts[type]['text'], img, title);
            Share.vkontakte(url, shareTexts[type]['text'], img, title);

            //_.bind(this.activateNextScreen, this);
            return false;
        });

        $('#game-share-prediction').on('click', _.bind(this.activateNextScreen, this));
        $('#game-get-discount').on('click', _.bind(this.activateNextScreen, this));
				$('.game-replay').on('click', function () { self.startGame(); });
        return this;
      }
    }.init();

    var loseScreenController = {
      element: $('#game-lose-screen'),
      show: function () {
        windowController.setHeight(this.element.height());

        var $element = this.element;
        $('.js-screen').hide(1, function () {
          //$element.addClass('active');
          $('.js-screen').removeClass('active');
          $element.show();
          $element.addClass('active');
        });

      },
      hide: function () {
        this.element.hide();
        this.element.removeClass('active');
      },
      init: function () {
        $('#game-replay-button').on('click', function () { self.startGame(); });
        return this;
      }
    }.init();

    var finalScreenController = {
      element: $('#game-final-screen'),
      show: function (error) {
        $('.js-hide-on-end').css({'opacity': 0});
				codeScreenController.element.addClass('hide-form-active');
        //$('.js-screen').removeClass('active');
        this.element.addClass('active');
      },
      showError: function () {
        this.element.find('h1').html('Что-то пошло нет так. Пожалуйста, попробуйте позже!');
        this.show();
      },
      hide: function () {
        $('.js-hide-on-end').css({'opacity': 1});
        this.element.removeClass('active');
      }
    };

		var helpController = {
			element: $('#game-help'),
			timeoutId: -1,
			activate: function () {
				var self = this;

				this.timeoutId = setTimeout(function () {
					self.element.animate({
						opacity: 1
					}, 300);
				}, 2000);

				$('.card').on('click', function () {
					clearTimeout(self.timeoutId);
					self.element.animate({
						opacity: 0
					}, 300);
				});
			}
		};

    this.active = false;
    //var $playButton = $('#game-play-button');
    var $playButton = $('.js-start-game');

    $playButton.on('click', function () {
      if ($(this).hasClass('h-small')) {
        var target = $(this).data('href');
        $('html, body').animate({
          scrollTop: $(target).offset().top
        }, 300);
      }

      ga('send', 'event', 'game', 'play');
      self.startGame();
    });

		$('#game-one-more-time').not('.js-no-replay').on('click', function () {
			self.startGame();
		});

		// New logic
    this.cardClicked = function (id) {
      timerController.stopTimer();
      if (!this.active) return false;
      var clicked = _.filter(this.getClicked(), function (elem) { return elem.clicked }),
        numberOfClicked = _.keys(clicked).length + 1;

      // If user selected 3 cards
      if (numberOfClicked > 3) return false;

      // This card is already opened, so do nothing
      if (!cards[id].clicked) {
        // No one card is opened
        cards[id].click(numberOfClicked);

        if (numberOfClicked == 3)
          self.endGame();
      }
    };

    // Old function
    this.cardClicked_ = function (id) {
      timerController.stopTimer();
      if (!this.active) return false;
      var openedAndNotMatched = _.filter(this.getOpened(), function (elem) { return !elem.matched }),
          numberOfOpened = _.keys(openedAndNotMatched).length;
      // This card is already opened, so do nothing
      if (!cards[id].opened) {
        // No one card is opened
        if (numberOfOpened == 0) {
          cards[id].open();
        }
        // One other card is opened, so open this and check for matches
        if (numberOfOpened == 1) {
          cards[id].open();
          if (cards[id].contentId == openedAndNotMatched[0].contentId) {
            cards[id].makeMatched();
            openedAndNotMatched[0].makeMatched();
            this.newMatch();
          } else {
            _.delay(function () {
              cards[id].close();
              openedAndNotMatched[0].close();
            }, 700);
          }
        }
        // Two cards are already opened, so close them and open this one
        if (numberOfOpened == 2) {
          _.each(openedAndNotMatched, function (elem) { elem.close(); });
          cards[id].open();
        }
      }
    };


    this.addCard = function (card) {
      cards[card.id] = card;
    };

    this.getOpened = function () {
      return _.filter(cards, function (elem) { return elem.opened });
    };

    this.getClicked = function () {
      return _.filter(cards, function (elem) { return elem.clicked });
    };

    this.getFirstClicked = function () {
      return _.filter(cards, function (elem) { return elem.clicked === 1 });
    };

    this.getMatched = function () {
      return _.filter(cards, function (elem) { return elem.matched });
    };

    this.newMatch = function() {
      matchesController.addMatch();
    };

    this.startGame = function () {
      if (!this.active) {
        loseScreenController.hide();
        predictionScreenController.hide();
				predictionScreenController.reset();
				codeScreenController.hide();
				finalScreenController.hide();
				$('#game-main-screen').show();
        this.resetGame();
        this.active = true;
        //$playButton.fadeOut(200);
        $('#game-play-button').fadeOut(200);
        timerController.startTimer();
				helpController.activate();
        this.openCards();
      }
    };

    // New logic
    this.endGame = function() {

      // GA goal
      ga('send', {
        'hitType': 'event',
        'eventCategory': 'game',
        'eventAction': 'finished',
        'eventValue': matchesController.matches
      });

      ga('send', 'event', 'game', 'result', matchesController.matches);

      timerController.stopTimer();
      this.active = false;

      var clicked = _.filter(this.getClicked(), function (elem) { return elem.clicked }),
          properties = {},
          property;

      if (clicked.length === 0) {
        loseScreenController.show();
      }
      else {
        $.each(clicked, function (index, card) {
          var letter = card.getLetter();
          properties[letter] = typeof properties[letter] === 'undefined' ? 1 : properties[letter] + 1;

        });

        var arr = Object.keys(properties).map(function (key) { return properties[key]; });
        var max = Math.max.apply(null, arr);

        if (max === 1) {
          var tempCard = _.filter(this.getFirstClicked(), function (elem) { return elem.clicked });
          property = tempCard[0].getLetter();
        }
        else {
          property = Object.keys(properties).reduce(function(a, b){ return properties[a] > properties[b] ? a : b });
        }

        //console.log(property);
        predictionScreenController.prepare(property);
        predictionScreenController.show();
      }

    };

    // Old function
    this.endGame_ = function() {

      // GA goal
      ga('send', {
        'hitType': 'event',
        'eventCategory': 'game',
        'eventAction': 'finished',
        'eventValue': matchesController.matches
      });

			ga('send', 'event', 'game', 'result', matchesController.matches);

      timerController.stopTimer();
      this.active = false;
      if (matchesController.matches < 1) {
        loseScreenController.show();
      } else {
        predictionScreenController.show();
      }
    };

    this.resetGame = function () {
      $('#game-main-screen').addClass('active');
      windowController.resetHeight();
      timerController.init();
      matchesController.init();
      cp.reset();
      _.each(cards, function (card) { card.reset(); });
    };

    this.openCards = function () {
      $('body').find('.card').addClass('opened');
    };
  };

  var numberOfCards = 12;
  var $parent = $('.game #cards-placeholder');
  var contentProvider = new ContentProvider(numberOfCards / 2)
  var gameController = new GameController(contentProvider);
  _(numberOfCards).times(function () { new Card(gameController, contentProvider).create($parent); });

};


$(function () {

  // *********************************************
  // Adding first slider
  // *********************************************
  $('#slider .slider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    lazyLoad: 'progressive',
    fade: true,
    cssEase: 'ease',
    prevArrow: $('#slider .prev-button'),
    nextArrow: $('#slider .next-button'),
    autoplay: true,
    autoplaySpeed: 2000,
  });

  // *********************************************
  // Adding first slider
  // *********************************************
  $('#lookbook .slider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    lazyLoad: 'progressive',
    fade: true,
    cssEase: 'ease',
    prevArrow: $('#lookbook .prev-button'),
    nextArrow: $('#lookbook .next-button'),
    autoplay: true,
    autoplaySpeed: 2000,
  });


  // *********************************************
  // Adding second slider
  // *********************************************
  $('#store .slider').slick({
    lazyLoad: 'ondemand',
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    cssEase: 'ease',
    prevArrow: $('#store .prev-button'),
    nextArrow: $('#store .next-button'),
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 300,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });


  // *********************************************
  // Playing Youtube video
  // *********************************************
  /*var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  var placeholder = document.getElementById('youtube-player');
  var id = placeholder.getAttribute('data-youtube-id');
  onYouTubeIframeAPIReady = function() {
    player = new YT.Player('youtube-player', {
      videoId: id,
      playerVars: { 'controls': 0 },
      events: {
        'onReady': bindToPlayButton,
      }
    });
  };

  function bindToPlayButton(event) {
    var video = event.target;
    $('.button-play').on('click', function () {
      video.playVideo();
      $(this).addClass('fade-out');
      setInterval(function () {
        $('.overlay').addClass('fade-out');
      }, 800);
      setInterval(function () {
        $('.overlay').addClass('hidden');
      }, 1000);
    });
  }*/

  // *********************************************
  // Adding game
  // *********************************************
  initGame();


  // *********************************************
  // Toggle menu
  // *********************************************
  $('.nav').on('click', function () {
    $('#menu').addClass('active');
  });

  $('#menu-arrow').on('click', function () {
    $('#menu').removeClass('active');
  });

  setInterval(function () { if ($(window).scrollTop() > 100) { $('#menu').removeClass('active') } }, 500);

  $('#want-gift').on('click', function () {
    $('html, body').animate({
        scrollTop: $("#rules").offset().top
    }, 500);
  });

  $('.scroll-to-top').on('click', function () {
    $('html, body').animate({
        scrollTop: 0
    }, 1000);
  });


  var howToPlayController = {
    unread: true,

    init: function () {
      var self = this;
      $('.how-to-play-close, #game-play-button').on('click', function () {
        self.unread = false;
        $('#how-to-play').removeClass('active');
      });
      setInterval(function () {
        var diff = Math.abs($(window).scrollTop() - $("#game").offset().top);
				if (self.unread) {
					return (diff < 150) ? $('#how-to-play').addClass('active') : $('#how-to-play').removeClass('active');
				}
      }, 500);

      $('#game-info').on('click', function () {
        self.unread = false;
        $('#how-to-play').addClass('active');
      });
      return this;
    }
  }.init();


/*	var videoTemplate = '<video autoplay loop id="video-cover"><source src="/images/gift.mp4" type="video/mp4"><source src="/images/gift.webm" type="video/webm"><source src="/images/gift.ogg" type="video/ogg"></video>';

	if (!jQuery.browser.mobile) {
		$('#video-container').html(videoTemplate);
		setInterval(function () {
			if ($(window).scrollTop() > $(window).height()) {
				$('#video-cover')[0].pause();
			} else {
				$('#video-cover')[0].play();
			}
		}, 500);
	}*/

  // *********************************************
  // Google analytics events
  // *********************************************
  $('#lookbook .slider').on('afterChange', function(event, slick, currentSlide){
    ga('send', {
      'hitType': 'event',
      'eventCategory': 'lookbook',
      'eventAction': 'nextphoto'
    });
  });

  $('.button-play').on('click', function () {
    ga('send', {
      'hitType': 'event',
      'eventCategory': 'lookbook',
      'eventAction': 'movie'
    });
  });

  $('.gotostore').on('click', function () {
    ga('send', {
      'hitType': 'event',
      'eventCategory': 'goto',
      'eventAction': 'store'
    });
  });

  // *********************************************
  // Scroll
  // *********************************************
  $('.js-scroll').click(function(e) {
    e.preventDefault();
    var target = $(this).data('href');
    $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 1000);
  });

});
