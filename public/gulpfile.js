var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCss = require('gulp-minify-css');


var paths = {
  sass: 'scss/*.scss',
  css: 'css/',
  js: 'js/',
}

gulp.task('default', ['watch']);

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['styles']);
  gulp.watch(paths.js + 'main.js', ['compress']);
});

gulp.task('styles', function() {
  gulp.src(paths.sass)
  .pipe(plumber({
    errorHandler: function (err) {
      console.log(err);
      this.emit('end');
    }
  }))
  .pipe(sass())
  .pipe(autoprefixer({
    browsers:  ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'ie >= 9'],
    cascade: false
  }))
  .pipe(minifyCss({compatibility: 'ie9'}))
  .pipe(gulp.dest(paths.css));
});

gulp.task('compress', function() {
  return gulp.src(paths.js + 'main.js')
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.js));
});
